////
////  jSonManager.swift
////  GiantBomb
////
////  Created by Leng Trang on 11/9/18.
////  Copyright © 2018 amo. All rights reserved.
////
//
//import Foundation
//
//protocol JsonManagerDelegate: class {
//    func didRecieveData(data: Data)
//}
//
//class JsonManager {
//    
//    weak var jsonDelegate: JsonManagerDelegate?
//    
//    init(apiKey: String, searchRequest: String){
//        let url = NSURL(string: "https://www.giantbomb.com/api/search/?api_key=\(apiKey)&format=json&query=%22\(searchRequest)%22&resources=game")
//        let request = NSURLRequest(url: url! as URL)
//        let config = URLSessionConfiguration.default
//        let session = URLSession(configuration: config)
//        
//        let task = session.dataTask(with: request as URLRequest, completionHandler: {(data, response, error) in
//            
//            if error != nil{
//                
//                print("Response: \(String(describing: error?.localizedDescription))")
//            }else{
//                
//                let jsonData = String(data: data!, encoding: String.Encoding.utf8)
//                
//                do {
//                    //                        let decodeData = Data(contentsOf: jsonData)
//                    //                        let jsonDecode = try JSONDecoder().decode(GiantBombSearch.self, from: data!)
//                }
//                catch {
//                    print("Error Decoding")
//                }
//                print("Data: \(String(describing: jsonData))")
//                
//            }
//        });
//        
//        // do whatever you need with the task e.g. run
//        task.resume()
//    }
//    
//}
//
//struct GiantBombSearch: Decodable {
//    let error: String
//    let limit: Int
//    let offeset: Int
//    let number_of_page_results: Int
//    let number_of_total_results: Int
//    let status_code: Int
//    let results: [Results]
//}
//struct Results: Decodable {
//    let aliases: String
//    let api_detail_url: String
//    let date_added: Date
//    let date_last_updated: Date
//    let deck: String
//    let description: String
//    let expected_release_day: Date
//    let expected_release_month: Date
//    let expected_release_quarter: Date
//    let expected_release_year: Date
//    let guid: String
//    let id: Int
//    let image: [ResultImage]
//    let image_tags: [ResultImageTag]
//    let name: String
//    let number_of_user_review: Int
//    let original_game_rating: [ResultOriginalGamerating]
//    let original_release_date: Date
//    let platform:[ResultPlatform]
//    let site_detail_url: String
//    let resource_type: String
//}
//
//struct ResultImage: Decodable {
//    let icon_url: String
//    let medium_url: String
//    let screen_url: String
//    let screen_large_url: String
//    let small_url: String
//    let super_url: String
//    let thumb_url: String
//    let tiny_url: String
//    let original_url: String
//    let image_tags: String
//}
//
//struct ResultImageTag: Decodable {
//    let api_detail_url: String
//    let name: String
//    let total: Int
//}
//
//struct ResultOriginalGamerating: Decodable {
//    let api_detail_url: String
//    let id: Int
//    let name: String
//}
//
//struct ResultPlatform: Decodable {
//    let api_detail_url: String
//    let id: Int
//    let name: String
//    let site_detail_url: String
//    let abbreviation: String
//}
//
