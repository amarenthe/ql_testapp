//
//  UIFormatter.swift
//  GiantBomb
//
//  Created by Leng Trang on 11/9/18.
//  Copyright © 2018 amo. All rights reserved.
//

import Foundation
import UIKit

struct UIFormattor {
    
    static func goButtonFormat(button: UIButton) -> UIButton {
    
        button.layer.cornerRadius = 10
        button.layer.borderWidth = 2
        button.layer.borderColor = UIColor.yellow.cgColor
        
        return button
    }
    
}
