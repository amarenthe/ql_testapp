//
//  ViewController.swift
//  GiantBomb
//
//  Created by Leng Trang on 11/9/18.
//  Copyright © 2018 amo. All rights reserved.
//

// NOTES: Please complete the application described below. We will be examining the structure and design of the code. There is no time limit, but try not to spend more than a few hours completing your app.
//1. Video Game Search
//Create an app that searches a video game database with a search string that the user inputs. For example, you may choose to use the Giant Bomb API (register for an API key at http://www.giantbomb.com/api/), where an example HTTP request for game search would be:
//http://www.giantbomb.com/api/search/?api_key=[API_KEY]&format=json&query="[SEARCH _TERM]"&resources=game
//The app should take input from the user, query the database, and display any results or errors to the user. At a minimum, display a thumbnail image (if available) and the name of each result. The flow and design of the app is important and should provide an intuitive user experience. Performance is also important, especially when displaying the images of the results.

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var apiKeyTxtFld: UITextField!
    @IBOutlet weak var enterGameNameTxtFld: UITextField!
    @IBOutlet weak var loadGame: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        apiKeyTxtFld.text = "39cbb66441e4ba63b580813ccbfc111d45d29a8f"
        enterGameNameTxtFld.text = "Halo"
        
        loadUIFormat()
    }

    
    @IBAction func onAPIKeyDidEndOnExit(_ sender: Any) {
    }
    
    @IBAction func onGameDidEndOnExit(_ sender: Any) {
    }
    
    @IBAction func onGoPressed(_ sender: Any) {
        // 39cbb66441e4ba63b580813ccbfc111d45d29a8f
        guard let apiKey = self.apiKeyTxtFld.text else {
            alert()
            return
        }
        guard let gamerSearch = self.enterGameNameTxtFld.text else {
            alert()
            return
        }
        if apiKey.count <= 0 || gamerSearch.count <= 0 {
            alert()
            return
        }
        
        let json = JsonManager(apiKey: apiKey, searchRequest: gamerSearch)
        
        json.jsonDelegate = self
    }
    
}

// Function for Loading UI Formats and Utility Function
extension ViewController {
    func loadUIFormat() {
        loadGame = UIFormattor.goButtonFormat(button: loadGame)
    }
    
    func alert() {
        let alertController = UIAlertController(title: "Incomplete Information", message: "Please enter API Key and or search topic", preferredStyle: .alert)
        
        let defaultAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(defaultAction)
        
        present(alertController, animated: true, completion: nil)
    }
}

// UI Events
extension ViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        return cell
    }
}

extension ViewController: JsonManagerDelegate {
    func didRecieveData(data: [Results]) {
        print("VC Data: \(data)")
    }
    
    
}
